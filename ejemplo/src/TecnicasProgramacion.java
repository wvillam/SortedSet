import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class TecnicasProgramacion {
    public static void main(String[] args) {

        //Se crea un nuevo Set y se le agregan 5 elementos que serán impresos posteriormente.
        //Set no tiene los elementos ordenados y los imprime en cualquier orden.
        Set<String> prueba= new HashSet<String>();
        prueba.add("5");
        prueba.add("2");
        prueba.add("3");
        prueba.add("Cosa");
        prueba.add("Mucho");

        System.out.println("Usando Set");

        for(String text : prueba){
            System.out.println(text);
        }

        System.out.println("--------------------");
        System.out.println("--------------------");

        //Se crea un SortedSet que será impreso posteriormente
        //La diferencia es que el SortedSet ordena los archivos de menor a mayor antes de imprimirlos
        //Y siempre los ordena constantemente aún cuando se hacen cambios.

        SortedSet<String> prueba2= new TreeSet<String>();
        prueba2.add("5");
        prueba2.add("2");
        prueba2.add("3");
        prueba2.add("Cosa");
        prueba2.add("Mucho");

        System.out.println("Usando SortedSet");

        for(String text : prueba2){
            System.out.println(text);
        }
    }
}
